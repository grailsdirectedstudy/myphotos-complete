submitTopic = function(id) {
    "use strict";
    Topics.get(id, function(topic) {
        var fd = new FormData();
        fd.append("topicName", topic.name);
        fd.append("photos", topic.photoKeys.length);

        var sync = barrier(topic.photoKeys.length, function() {
            // Submit the topic and then redirect back to the topic's page
            $.ajax({
                type: "POST",
                url: "/myphotos/OTopic/submitTopic",
                data: fd,
                processData: false,
                contentType: false
            }).done(function() {
                Topics.delete(id, function() {
                    window.location.href = "/myphotos/topic";
                });
            });
        });

        // Load the photo from localforage and store it in our FormData Object
        function loadPhoto(photoIndex) {
            localforage.getItem(topic.photoKeys[photoIndex], function(err, photo) {
                fd.append("photo" + photoIndex, photo);
                sync();
            });
        }

        for (var i = 0; i < topic.photoKeys.length; i++) {
            loadPhoto(i);
        }
    });
};
