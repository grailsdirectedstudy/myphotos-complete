//This file contains code to view a single topic's page
$(function() {
	"use strict";

	if (online) {
		$("title").text("myPhotos - View Topic");
	}

	var photoTemplate = '\
		<div class = "row"> \
		<div class = "col-xs-12"> \
		<div class = "image-container"> \
		<img class = "full-image" /> \
		<button class = "photoDelLink del-image pull-right btn btn-xs btn-danger"> \
		Delete \
		</button> \
		</div> \
		</div> \
		</div>';

	// The topic ID we are attempting to view is in the URL as an
	// argument so parse it out and view the data for that topic
	var id = getId();

	var submitButtonTemplate = '<button id = "submitTopic" class = "btn-primary btn btn-sm">Submit Topic</button>';

	// Fill in the links for our buttons and add click handler
	$("#addPhotoLink").attr("href", "/myphotos/OTopic/addPhotos#id=" + id);
	$("#edLink").attr("href", "/myphotos/OTopic/edit#id=" + id);

	$("#deLink").click(function() {
		Topics.delete(id, function() {
			window.location.href = "/myphotos/topic";
		});
	});

	// Want to add our submit button if we can submit to server
	if (online) {
		$("#buttonGroup").append(submitButtonTemplate);
		$("#submitTopic").click(function() {
			submitTopic(id);
		});
	}

	Topics.get(id, function(topic) {
		// First add in the bar with the topic name
		$("#topicName").append(topic.name);

		// Appends a single photo to our page in async fashion
		function appendPhotoRow(index) {
			localforage.getItem(topic.photoKeys[index], function(err, photo) {
				// Append the photo row to the main body and fill in
				// the src with photo data
				var elem = $(photoTemplate);
				elem.find(".photoDelLink").click(function() {
					var photoId = topic.photoKeys[index];
					topic.photoKeys.splice(index, 1);

					var sync = barrier(2, function() {
						// Force a refresh after deleting an image to update
						// the dom.  This could also be done by simply removing
						// this element from the container
						window.location.reload();
					});
					localforage.removeItem(photoId, sync);
					localforage.setItem(id, topic, sync);
				});


				var reader = new FileReader();
				reader.onload = function() {
					// Fill in the new photo element and append it to the page
					elem.find(".full-image").attr("src", reader.result);
					$("#photoContainer").append(elem);
				};
				
				// Fred + Stephen (5/6/15): Added coded to convert ArrayBuffers to blobs
				// Fixes blob storage issue for IndexedDB on Safari
				reader.readAsDataURL(new Blob([photo]));
				
			});
		}

		// Append all the photos under this topic
		for (var i = 0; i < topic.photoKeys.length; i++) {
			appendPhotoRow(i);
		}
	});
});
