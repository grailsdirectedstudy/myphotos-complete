// Some utility functions

// Generate guid from stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
// Probability of collision is low, so we don't do checking
guid = function() {
    "use strict";
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString()
            .substring(1);
    }

    return s4() + s4() + "-" + s4() + "-" + s4() + "-" +
        s4() + "-" + s4() + s4() + s4();
};

// Get the id of a topic from the url
getId = function() {
    "use strict";
    var uri = window.location.href;
    return uri.substring(uri.indexOf("id=") + 3);
};

// Make a barrier for synchronizing async callbacks
barrier = function(n, callback) {
    "use strict";
    return function() {
        n--;
        if (n === 0) {
            callback();
        }
    };
};
