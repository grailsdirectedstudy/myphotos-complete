// This is the javascript for the create topic page

$(function() {
    "use strict";
    $("#store").click(function() {
        // Create a new topic object and store it in localforage
        // then redirect the user to that topic's page, just like the
        // online version
        var photoInput = $("#photos")[0];
        var topic = new Topic($("#topicName").val());

        for (var i = 0; i < photoInput.files.length; i++) {
            topic.addPhoto(photoInput.files[i]);
        }

        topic.save(function(id) {
            window.location.href = "/myphotos/OTopic/view#id=" + id;
        });
    });
});
