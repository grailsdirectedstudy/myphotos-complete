$(function() {
    "use strict";

    // Set the title properly
    if (online) {
        $("title").text("myPhotos - Add Photos");
    }

    $("#upload").click(function() {

        var topicId = getId();
        var photos = [];

        // Save a new photo in local forage for each photo
        // we get and generate it's id
        var photoInput = $("#photos")[0];
        var photosLeft = photoInput.files.length;

        // Get the topic and add those keys that we just created
        // to its list of keys.
        var finish = function() {
            Topics.get(topicId, function(topic) {
                for (var photoIndex = 0; photoIndex < photos.length; photoIndex++) {
                    topic.photoKeys.push(photos[photoIndex]);
                }
                localforage.setItem(topicId, topic, function() {
                    // Redirect back to the view topic page
                    window.location.href = "/myphotos/OTopic/view#id=" + topicId;
                });
            });
        };

        var sync = barrier(photosLeft, finish);

        for (var i = 0; i < photoInput.files.length; i++) {
            // Save all the photos into localforage that the user wanted to
            // upload.  Call sync as the callback to local forage to ensure
            // we only do the redirect after the topic and it's photos
            // have been saved.
        	
            
            // Fred + Stephen (5/6/15): Added coded to convert blobs to ArrayBuffers
            // Fixes blob storage issue for IndexedDB on Safari
        	var reader = new FileReader();
        	reader.addEventListener("loadend", function() {
        	   // reader.result contains the contents of blob as a typed array
        		var photoId = guid();
                localforage.setItem(photoId, reader.result, sync);
                photos.push(photoId);
                
        	});
        	reader.readAsArrayBuffer(photoInput.files[i]);
        }
    });
});
