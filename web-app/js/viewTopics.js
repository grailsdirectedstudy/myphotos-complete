// This file contains code to view a single topic
$(function() {
    "use strict";

    // Template for a topic to be displayed.  Shows the topic name
    // (including a link to the topic) and the thumbnail
    var topicTemplate = ' \
        <div class = "col-md-3 col-xs-12"> \
            <h3><a class = "topicLink"></a></h3> \
            <div style = "height: 20%; overflow: hidden"> \
                <div class = "thumbnail"> \
                    <img class = "topicImage" /> \
                </div> \
            </div> \
        </div>';

    // Jumbotron for if we are offline and there are no offline topics
    var jumbotronOfflineTemplate = ' \
        <div class = "jumbotron"> \
            <h1>Nothing here yet!</h1> \
            <p><a href = "/myphotos/topic/create">Create a Topic</a> to get started!</p> \
        </div>';

    // Jumbotron for if we are online and there are no offline topics
    var jumbotronOnlineTemplate = '\
        <div class = "jumbotron"> \
            <h1>Nothing here</h1> \
            <p>You have no local topics to submit</p> \
        </div>';

    Topics.getAll(function(topicObjects) {
        var noOfflineTopics = (topicObjects == null || topicObjects.length === 0);

        if (noOfflineTopics && !online) {
            // There are no topics, show the jumbotron
            $("#topicContainer").append(jumbotronOfflineTemplate);
        } else if (noOfflineTopics && online) {
            // Let the user know that there isn't anything they need to submit
            $("#topicContainer").append(jumbotronOnlineTemplate);
        } else {
            var rowList = [];

            var callsLeft = topicObjects.length;
            var sync = barrier(callsLeft, function() {
                // Assemble all the rows
                for (var rowIndex = 0; rowIndex < rowList.length; rowIndex++) {
                    $("#topicContainer").append(rowList[rowIndex]);
                }
            });

            // Show these in the 4 groupings like with the tag
            // library
            var shownTotal = 0;
            var rowsTotal = topicObjects.length / 4;

            /* General Architectural note:
             *
             * The below code is somewhat dense, so here's the big picture idea.  We have a bunch
             * of topics that need to be arranged into rows with 4 topics per row.  Each topic
             * may possibly have an image that we want to load up for its thumbnail.  Lets break
             * this into steps.
             *
             * 1. For each row, we want to load in 4 topics
             *  a. For each topic in that row, we have to check if it has an image or it doesn't.  If this
             *     topic has no images then we can just be done with it, append it to the row, and put in
             *     a placeholder image (curtosey of placehold.it.  If we have to load an image things
             *     get a little more complicated.
             *
             *     The images have to be loaded out of local forage.  In order to keep all our calls in sync,
             *     we want to load all the images in a batch once we know how many images we have.  For that
             *     reason, if we have to load an image for this topic, we deffer the loading until later.  Save
             *     the context (consisting of the row, topic template, and the photo key) and put it on our
             *     queue of images to load later.
             *  b. We need to call sync once for each topic that we have to load for the view.  We can call
             *     sync once for each topic that had a placeholder image.
             */
            var imagesToLoadFromLocalforage = [];

            for (var i = 0; i < rowsTotal; i++) {
                var row = $('<div class = "row"></div>');
                var imagesToLoadFromLocalforageForThisRow = 0;

                for (var j = 0; j < 4 && shownTotal < topicObjects.length; j++) {
                    var currentImage = $(topicTemplate);
                    currentImage.find(".topicLink").append(topicObjects[shownTotal].name);
                    currentImage.find(".topicLink").attr("href", "/myphotos/OTopic/view#id=" + topicObjects[shownTotal].key);

                    if (topicObjects[shownTotal].photoKeys.length === 0) {
                        currentImage.find(".topicImage").attr("src", "http://placehold.it/250x250");
                        row.append(currentImage);
                    } else {
                        imagesToLoadFromLocalforage.push({
                            "currentImage": currentImage,
                            "row": row,
                            "photo": topicObjects[shownTotal].photoKeys[0]
                        });
                        imagesToLoadFromLocalforageForThisRow += 1;
                    }

                    shownTotal++;
                }

                rowList.push(row);

                // For every non-defered image, we want to call sync.  This is so that if the row
                // does not contain any deffered images, it will still get synced up normally.  Additionally,
                // this has to be done AFTER rowlist gets the new row because otherwise we may call sync
                // and have the last callback trigger before everything is in place.
                for (var syncCallCounter = 0; syncCallCounter < 4 - imagesToLoadFromLocalforageForThisRow; syncCallCounter++) {
                    sync();
                }
            }

            /* More architecture
             *
             * So now we can load all our deffered images.  For each image we want to apply the blob
             * that we load from localforage to the context that we have saved from earlier.  After we
             * apply the blob to the context, we call sync and the whole row will be added at once.
             */
            var loadImage = function(index) {
                localforage.getItem(imagesToLoadFromLocalforage[index].photo, function(err, photo) {
                    function appendImage(imageUrl) {
                        imagesToLoadFromLocalforage[index].currentImage.find(".topicImage").attr("src", imageUrl);
                        imagesToLoadFromLocalforage[index].row.append(imagesToLoadFromLocalforage[index].currentImage);
                        sync();
                    }

                    var reader = new FileReader();
                    reader.onload = function() {
                        appendImage(reader.result);
                    };

                    reader.readAsDataURL(new Blob([photo]));
                });
            };

            // Now load in all the images
            for (var imageIndex = 0; imageIndex < imagesToLoadFromLocalforage.length; imageIndex++) {
                loadImage(imageIndex);
            }
        }
    });
});
