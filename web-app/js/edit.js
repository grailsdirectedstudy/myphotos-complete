$(function() {
    "use strict";
    // Set the title properly
    if (online) {
        $("title").text("myPhotos - Edit");
    }

    var topicId = getId();
    Topics.get(topicId, function(topic) {
        $("#topicName").attr("value", topic.name);
    });

    $("#submit").click(function() {
        Topics.get(topicId, function(topic) {
            topic.name = $("#topicName").val();
            localforage.setItem(topicId, topic, function() {
                window.location.href = "/myphotos/OTopic/view#id=" + topicId;
            });
        });
    });
});
