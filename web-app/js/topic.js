Topic = function(name) {
    "use strict";
    this.name = name;
    this.photos = {};
    this.key = guid();

    // Add a photo (a Blob object) to this topic
    this.addPhoto = function(photoFile) {
        var id = guid();
        this.photos[id] = photoFile;
    };

    // Save this topic into localforage
    this.save = function(callback) {
        var photoKeys = Object.keys(this.photos);

        // There are n + 2 calls left.  One for each
        // photo, one for the topic object being saved
        // into local forage, and one for the global topic
        // object saving us to its array.
        var callsRemaining = photoKeys.length + 2;

        // Synchronization
        var boundCallback = callback.bind(this, this.key);
        var sync = barrier(callsRemaining, boundCallback);

        var serialized = this.serialize();

        // Save ourselves to localforage and also
        // save the photos in our library under their keys
        localforage.setItem(this.key, serialized, sync);

        for (var i = 0; i < photoKeys.length; i++) {
            localforage.setItem(photoKeys[i], this.photos[photoKeys[i]], sync);
        }

        // Save to the global topics object so it knows about our existance
        Topics.push(this, sync);
    };

    // We don't want to serialize the photo (Blob object) inside
    // the topic for performance reasons.  Instead, just save all
    // the keys.  This function creates the serialized version of our
    // topic object that we put into local forage
    this.serialize = function() {
        var serialized = {};

        serialized.name = this.name;
        serialized.photoKeys = [];
        serialized.key = this.key;

        // Do a copy here so we don't loose references
        var origionalKeys = Object.keys(this.photos);

        for (var i = 0; i < origionalKeys.length; i++) {
            serialized.photoKeys.push(origionalKeys[i]);
        }

        return serialized;
    };
};

// Global object for dealing with all topics at the same time
Topics = {};
Topics.getAll = function(cb) {
    "use strict";
    localforage.getItem("topics", function(err, data) {
        if (data == null || data.length === 0 ) {
            return cb([]);
        }

        // Topics holds onto a list of topic keys, we want
        // to look them up here
        var topicObjects = [];
        var sync = barrier(data.length, function() {
            cb(topicObjects);
        });

        var pushCallback = function(itemError, topic) {
            topicObjects.push(topic);
            sync();
        };

        for (var i = 0; i < data.length; i++) {
            localforage.getItem(data[i], pushCallback);
        }
    });
};

// T is the topic to push onto the list of topics and cb is
// an optional callback to be fired after the operation takes
// place
Topics.push = function(t, cb) {
    "use strict";
    localforage.getItem("topics", function(err, data) {
        var currentTopics = data;
        if (data == null) {
            currentTopics = [];
        }
        currentTopics.push(t.key);

        localforage.setItem("topics", currentTopics, function(setErr, itemData) {
            if (cb !== undefined) {
                cb(itemData);
            }
        });
    });
};

Topics.get = function(id, cb) {
    "use strict";
    localforage.getItem(id, function(err, data) {
        cb(data);
    });
};

Topics.delete = function(id, cb) {
    "use strict";
    // Get the item, remove it and then also remove all the keys
    // for the photos associated with it
    localforage.getItem(id, function(err, topic) {

        var callsRemaining = topic.photoKeys.length + 2;
        var sync = barrier(callsRemaining, cb);

        // Have to remove the topic and its photos
        localforage.removeItem(id, sync);
        for (var i = 0; i < topic.photoKeys.length; i++) {
            localforage.removeItem(topic.photoKeys[i], sync);
        }

        // Also remove it from out list of topic keys
        localforage.getItem("topics", function(topicsError, topics) {
            topics.splice(topics.indexOf(id), 1);
            localforage.setItem("topics", topics);
            sync();
        });
    });
};
