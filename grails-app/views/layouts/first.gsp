<!DOCTYPE html>
<html manifest="/myphotos/manifest">
    <head>
        <title><g:layoutTitle/></title>
        <g:layoutHead/>
        <g:render template="/assets" />
        <script type="text/javascript">
        function onUpdateReady() {
        	  alert('found new version!');
        	}
        	window.applicationCache.addEventListener('updateready', onUpdateReady);
        	if(window.applicationCache.status === window.applicationCache.UPDATEREADY) {
        	  onUpdateReady();
        	}
  		</script>
    </head>
    <body>
        <g:render template="/navbar" />
        <g:layoutBody/>
        <br /><br /> <!-- Add some whitespace at the bottom -->
    </body>
</html>
