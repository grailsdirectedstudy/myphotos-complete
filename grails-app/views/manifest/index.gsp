CACHE MANIFEST

#version !!!!!!!

CACHE:
/myphotos/js/jquery-2.1.3.min.js
/myphotos/js/bootstrap.min.js
/myphotos/css/bootstrap.min.css
/myphotos/css/style.css
/myphotos/js/localforage.js
/myphotos/js/create.js
/myphotos/js/edit.js
/myphotos/js/topic.js
/myphotos/js/addPhotos.js
/myphotos/js/util.js
/myphotos/js/view.js
/myphotos/js/viewTopics.js
/myphotos/js/submitTopic.js

FALLBACK:
# Explicitly cache both "versions" of the homepage
/myphotos /myphotos/

# Offline pages
/myphotos/topic/create /myphotos/OTopic/create
/myphotos/topic /myphotos/OTopic
/myphotos/topic/view /myphotos/OTopic/view
/myphotos/topic/addPhotos /myphotos/OTopic/addPhotos
/myphotos/topic/edit /myphotos/OTopic/edit

# A copy of the above but with / added to the end,
# it might pay off to be paranoid and this is cheap
# to cache
/myphotos/topic/create/ /myphotos/OTopic/create
/myphotos/topic/ /myphotos/OTopic
/myphotos/topic/view/ /myphotos/OTopic/view
/myphotos/topic/addPhotos/ /myphotos/OTopic/addPhotos
/myphotos/topic/edit/ /myphotos/OTopic/edit

# For telling if we are online or offline in the javascript
/myphotos/js/online.js /myphotos/js/offline.js

NETWORK:
*
