<!DOCTYPE html>
<html>
    <head>
        <title>myPhotos (Offline) - Upload pictures</title>
        <meta name = "layout" content = "main">
    </head>
    <body>
        <div class = "container-fluid">
            <div class = "row">
                <div class = "col-xs-6 col-xs-offset-3">
                    <form onSubmit = "return false;" action="javascript: return false;">
                        <div class = "form-group">
                            <label for = "photos">Photos</label>
                            <input id = "photos" type = "file" accept = "image/*" name = "photos" capture multiple>
                        </div>
                        <button id = "upload" type = "submit" class = "btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>

        <g:external dir="js" file="util.js" />
        <g:external dir="js" file="topic.js" />
        <g:external dir="js" file="addPhotos.js" />
    </body>
</html>
