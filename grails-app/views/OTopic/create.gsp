<!DOCTYPE html>
<html>
    <head>
        <title>myPhotos (Offline) - Create a Topic</title>
        <meta name = "layout" content = "main">
    </head>
    <body>
        <div class = "container-fluid">
            <div class = "row">
                <div class = "col-xs-6 col-xs-offset-3">
                    <form onSubmit = "return false;" action="javascript:return false;">
                        <div class = "form-group">
                            <label for = "topicName">Topic Name</label>
                            <input name = "topicName" type = "text" class = "form-control" id = "topicName" />
                        </div>
                        <div class = "form-group">
                            <label for = "photos">Photos</label>
                            <input type = "file" name = "photos" id = "photos" multiple>
                            <p class = "help-block">Uploading photos is optional</p>
                        </div>
                        <button id = "store" type = "submit" class = "btn btn-default">Store</button>
                    </form>
                </div>
            </div> <!-- row -->
        </div> <!-- Container -->

        <g:external dir="js" file="util.js" />
        <g:external dir="js" file="topic.js" /> 
        <g:external dir="js" file="create.js" />
    </body>
</html>
