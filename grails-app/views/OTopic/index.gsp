<!DOCTYPE html>
<html>
    <head>
        <title>myPhotos (Offline) - Topics</title>
        <meta name = "layout" content = "main">
    </head>
    <body>
        <div class = "container-fluid">
            <div class = "col-sm-10 col-sm-offset-1" id = "topicContainer">
            </div>
        </div> <!-- Container -->

        <g:external dir="js" file="util.js" />
        <g:external dir="js" file="topic.js" /> 
        <g:external dir="js" file="viewTopics.js" />
    </body>
</html>
