
<!DOCTYPE html>
<html>
    <head>
        <title>myPhotos (Offline) - Editing</title>
        <meta name = "layout" content = "main">
    </head>
    <body>
        <div class = "container-fluid">
            <div class = "row">
                <div class = "col-xs-6 col-xs-offset-3">
                    <form onSubmit = "return false;" action = "javascript: return false;">
                        <div class = "form-group">
                            <label for = "topicName">Topic Name</label>
                            <input name = "topicName" type = "text" class = "form-control" id = "topicName"></input>
                        </div>
                        <button id = "submit" type = "submit" class = "btn btn-default">Confirm</button>
                    </form>
                </div>
            </div>
        </div>

        <g:external dir="js" file="util.js" />
        <g:external dir="js" file="topic.js" /> 
        <g:external dir="js" file="edit.js" />
    </body>
</html>
