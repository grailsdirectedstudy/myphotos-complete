<!DOCTYPE html>
<html>
    <head>
        <title>myPhotos (Offline) - View Topic</title>
        <meta name = "layout" content = "main">
        <asset:stylesheet href = "style.css" />
    </head>
    <body>
        <div class = "container">
            <div class = "row">
                <div class = "col-xs-12"><h1 id = "topicName"></h1></div>

                <div class = "col-xs-12">
                    <div id = "buttonGroup" style = "margin-top: 25px;">
                        <a class = "btnlink" id = "addPhotoLink" href = "">
                            <button class = "btn btn-sm">Add Photos</button>
                        </a>
                        <a class = "btnlink" id = "edLink" href = "">
                            <button class = "btn btn-sm">Edit Topic</button>
                        </a>
                        <button id = "deLink" class = "btn btn-sm btn-danger">Delete Topic</button>
                    </div>
                </div>
            </div><br />

            <div id = "photoContainer"> 
            </div>

        </div> <!-- Container -->
        <g:external dir="js" file="util.js" />
        <g:external dir="js" file="topic.js" /> 
        <g:external dir="js" file="submitTopic.js" />
        <g:external dir="js" file="view.js" />
    </body>
</html>
