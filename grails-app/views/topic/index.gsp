<!DOCTYPE html>
<html>
    <head>
        <title>myPhotos - Topics</title>
        <meta name = "layout" content = "main">
    </head>
    <body>
        <div class = "container-fluid">
            <div class = "col-sm-10 col-sm-offset-1" id = "topicContainer">
                <h1 id = "localTopicsHeading">Topics Stored Locally</h1>
            </div>

            <div class = "col-sm-10 col-sm-offset-1">
                <h1 id = "serverTopicsHeading">Topics Stored on Server</h1>
                <mp:topicCollection />
            </div>
        </div> <!-- Container -->

        <g:external dir="js" file="util.js" />
        <g:external dir="js" file="topic.js" /> 
        <g:external dir="js" file="viewTopics.js" />
    </body>
</html>
