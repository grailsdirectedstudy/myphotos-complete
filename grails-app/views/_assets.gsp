<g:external dir="css" file="bootstrap.min.css" />
<g:external dir="css" file="style.css" />

<!-- Javascript in header so that page javascript gets to use it -->
<g:external dir="js" file="jquery-2.1.3.min.js" />
<g:external dir="js" file="bootstrap.min.js" />
<g:external dir="js" file="localforage.js" />
<g:external dir="js" file="online.js" />
